require 'test_helper'

class ConverterControllerTest < ActionDispatch::IntegrationTest

  test "should get redirect to auth" do
    get converter_index_url
    assert_redirected_to('/users/sign_in')
  end

end
