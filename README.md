# Description 
Simple RoR wrapper for FFmpeg
* Converting audio files to another format w/ bitrate selection
* Rotating video files without re-encoding

# Dependencies:
* FFmpeg
* Ruby 2.6.3
* Rails 6.0.1

