Rails.application.routes.draw do
  devise_for :users
  root 'converter#index'
  
  post 'converter/upload'
  get 'converter/main'
  get 'converter/out_formats'
  post 'converter/convert'
  post 'converter/download'
  post 'converter/cleaner'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
