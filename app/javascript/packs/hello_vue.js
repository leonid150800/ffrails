import Vue from 'vue/dist/vue.esm'
import App from '../app.vue'
import VModal from 'vue-js-modal'

Vue.use(VModal, { dialog: true })

document.addEventListener('DOMContentLoaded', () => {
  const app = new Vue({
    el: '#hello',
    data: {
    },
    components: { App }
  })
})