# frozen_string_literal: true

require 'open3'

class ConverterController < ApplicationController
  before_action :authenticate_user!
  skip_before_action :verify_authenticity_token

  def initialize
    @formats = {
      audio: %w[mp3 opus ogg flac],
      video: %w[mkv mp4 mov 3gp avi]
    }
  end

  def index; end

  def main;  end

  def upload
    session[:foo] = 'bar'
    session_id = request.session_options[:id]
    Upload.create token: session_id, file: params[:file].original_filename
    p session_id
    unless File.directory? Rails.root.join('storage', session_id)
      Dir.mkdir Rails.root.join('storage', session_id)
    end
    File.open(Rails.root.join('storage', session_id, params[:file].original_filename), 'wb') do |file|
      file.write(params[:file].read)
    end
    render json: params[:file].original_filename
  end

  def out_formats
    render json: @formats
  end

  def convert
    session_id = request.session_options[:id]
    filename = Upload.where(['token = ? and file = ?', session_id, params[:file]]).last.file
    out_format = params[:format].split[0]

    in_filename, in_extension = filename.scan(/(.*)(\.[^.]+)$/)[0]
    out_filename = in_extension[1..] == out_format ? "#{in_filename}_converted" : in_filename
    out_file = "#{out_filename}.#{out_format}"
    case params[:converter_action]
    when 'convert-audio'
      convert_audio session_id, filename, out_format, out_file, params[:bitrate]
    when 'rotate-video'
      rotate_video session_id, filename, params[:angle], out_file
    end
    render json: out_file
  end

  def download
    session_id = request.session_options[:id]
    file = params[:file]
    filename = file.scan(/(.*)(\.[^.]+)$/)[0][0].gsub('_converted', '')
    if Upload.where(['token = ? and file like ?', session_id, "#{filename}%"]).exists?
      response.headers['Content-Length'] = Rails.root.join('storage', session_id, file).size.to_s
      send_file Rails.root.join('storage', session_id, file), type: 'application/force-download'
    else
      render json: 'Error'
    end
  end

  def rotate_video(session_id, filename, deg, out_file)
    Open3.capture2('ffmpeg', '-y', '-i', Rails.root.join('storage', session_id, filename).to_s, '-c', 'copy', '-map_metadata', '0', '-metadata:s:v', "rotate=#{deg}", Rails.root.join('storage', session_id, out_file).to_s)
  end

  def convert_audio(session_id, filename, out_format, out_file, bitrate = nil)
    Open3.capture2('ffmpeg', '-y', '-i', Rails.root.join('storage', session_id, filename).to_s, '-f', out_format, '-b:a', bitrate ? "#{bitrate}k" : '320', Rails.root.join('storage', session_id, out_file).to_s)
  end

  def cleaner
    render json: Upload.clean
  end
end
