class Upload < ApplicationRecord
    def self.clean

        time = 10.minutes.ago

        files = Upload.where('updated_at < ?',  time).each do|e|
            if File.directory? Rails.root.join('storage', e.token)
                Dir.glob("#{Rails.root.join('storage', e.token)}/*").each{|file| File.delete(file) if file.match(Regexp.quote(e.file)[/.+\./])}
            end
        end
    end
end
